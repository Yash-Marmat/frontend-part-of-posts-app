import {
  GET_POSTS,
  GET_POSTS_SUCCESS,
  GET_POSTS_ERROR,
  DELETE_POSTS,
  CREATE_POSTS,
  UPDATE_POSTS,
} from "../actions/actionTypes";

const initialState = {
  loading: false,
  posts: [],
  error: "",
};

export const postsReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_POSTS:
      return {
        ...state,
        loading: true,
      };
    case GET_POSTS_SUCCESS:
      return {
        ...state,
        loading: false,
        posts: action.payload,
      };
    case GET_POSTS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case DELETE_POSTS:
      return {
        ...state,
        posts: state.posts.filter((post) => post.id !== action.payload),
      };
    case CREATE_POSTS:
      return {
        ...state,
        posts: [...state.posts, action.payload],
      };
    case UPDATE_POSTS:
      // condition for updates
      const updates = state.posts.map((post) => {
        if (post.id === action.payload.id) {
          return {
            id: action.payload.id,
            title: action.payload.title,
            content: action.payload.content,
          };
          // or --> return { ...post, ...action.payload };
        }
        return post; // the previous state of blog (if the condition doesn't match)
      });

      return {
        ...state,
        posts: updates,
      };
    default:
      return state; // original state
  }
};

export default postsReducer;

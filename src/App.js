import "./App.css";
import React, { Component, Fragment } from "react";
import { Provider } from "react-redux";
import Posts from "./components/Posts";
import store from "./store";
import { HashRouter as Router, Route, Switch } from "react-router-dom";
import Login from "./components/accounts/login";
import Register from "./components/accounts/Register";
import Navbar from "./components/Navbar";
import PrivateRoute from "./components/PrivateRoute";
import { loadUser } from "./actions/auth";
import "bootstrap/dist/css/bootstrap.min.css";

class App extends Component {
  // lifecycle method
  componentDidMount() {
    store.dispatch(loadUser());
  }

  render() {
    return (
      <div>
        <Provider store={store}>
          <Router>
            <Fragment>
              <div className="container">
                <Navbar />
                <Switch>
                  <PrivateRoute exact path="/" component={Posts} />
                  <Route exact path="/register" component={Register} />
                  <Route exact path="/login" component={Login} />
                </Switch>
              </div>
            </Fragment>
          </Router>
        </Provider>
      </div>
    );
  }
}

export default App;

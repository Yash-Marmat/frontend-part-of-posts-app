import { combineReducers } from "redux";
import postsReducer from "./reducers/postsReducer";
import auth from "./reducers/auth";

const rootReducer = combineReducers({
  post: postsReducer,
  auth,
});

export default rootReducer;

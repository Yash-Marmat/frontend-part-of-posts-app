import React, { Component } from "react";
import { connect } from "react-redux";
import { createPosts } from "../actions/postsAction";

export class CreatePosts extends Component {
  // toggle creation of new post
  togglePost = () => {
    this.props.toggleNewPost(); // from Posts.js
  };

  // handle form submission
  onSubmit = (e) => {
    e.preventDefault();
    const title = this.getTitle.value;
    const content = this.getContent.value;
    const postData = { title, content };
    this.props.createPosts(postData);

    // setting the fields back to empty once the form is submitted
    this.getTitle.value = "";
    this.getContent.value = "";
    this.togglePost();
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="post title"
            ref={(input) => (this.getTitle = input)}
            value={this.getTitle}
            required
          />
          <br />
          <textarea
            placeholder="content here"
            ref={(input) => (this.getContent = input)}
            value={this.getContent}
            required
          />
          <br />
          <button type="submit">save</button>
          <button onClick={this.toggleNewPost}>cancel</button>
        </form>
      </div>
    );
  }
}

export default connect(null, { createPosts })(CreatePosts);

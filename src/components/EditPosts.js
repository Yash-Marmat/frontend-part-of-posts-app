import React, { Component } from "react";
import { connect } from "react-redux";
import { updatePosts } from "../actions/postsAction";

export class EditPosts extends Component {
  // handle form submission
  onSubmit = (e) => {
    e.preventDefault();
    const title = this.getTitle.value;
    const content = this.getContent.value;
    const postData = { title, content };
    this.props.updatePosts(this.props.cloneId, postData);

    // setting the fields back to empty once the form is submitted
    this.getTitle.value = "";
    this.getContent.value = "";
    this.toggleEdits();
  };

  // toggle Edit Form
  toggleEdits = () => {
    this.props.toggleEditing(); // from Posts.js
  };

  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            type="text"
            placeholder="post title"
            ref={(input) => (this.getTitle = input)}
            value={this.getTitle}
            defaultValue={this.props.cloneTitle}
            required
          />
          <br />
          <textarea
            placeholder="content here"
            ref={(input) => (this.getContent = input)}
            value={this.getContent}
            defaultValue={this.props.cloneContent}
            required
          />
          <br />
          <button type="submit">save</button>
          <button onClick={this.toggleEdits}>cancel</button>
        </form>
      </div>
    );
  }
}

export default connect(null, { updatePosts })(EditPosts);

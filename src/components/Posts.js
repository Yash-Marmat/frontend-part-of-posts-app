import React, { Component } from "react";
import { connect } from "react-redux";
import { getPosts, fetchPostsData, deletePosts } from "../actions/postsAction";
import CreatePosts from "./CreatePosts";
import EditPosts from "./EditPosts";

export class Posts extends Component {
  state = {
    newPost: false,
    editing: false,
    cloneId: 0,
    cloneTitle: "",
    cloneContent: "",
  };

  // toggle new post form
  toggleNewPost = () => {
    this.setState({
      newPost: !this.state.newPost,
    });
  };

  // lifecycle method (calling GET_POSTS and backend API)
  componentDidMount() {
    this.props.getPosts();
    this.props.fetchPostsData();
  }

  // toggle editing
  toggleEditing = () => {
    this.setState({
      editing: !this.state.editing,
    });
  };

  // handle edits (contains data for the updation)
  handleEdits = (post) => {
    this.setState({
      cloneId: post.id,
      cloneTitle: post.title,
      cloneContent: post.content,
    });
    this.toggleEditing(); // now editing = true (will open EditPosts Form)
  };

  render() {
    console.log("loading is", this.props.postsData.loading);
    return (
      <div>
        {this.props.postsData.loading ? (
          <h3>Loading...</h3>
        ) : (
          <div>
            {this.props.postsData.error ? (
              this.props.postsData.error
            ) : (
              <div>
                {this.state.newPost ? (
                  <CreatePosts toggleNewPost={this.toggleNewPost} />
                ) : (
                  <div>
                    {this.state.editing ? (
                      <EditPosts
                        cloneId={this.state.cloneId}
                        cloneTitle={this.state.cloneTitle}
                        cloneContent={this.state.cloneContent}
                        toggleEditing={this.toggleEditing}
                      />
                    ) : (
                      <div>
                        <button onClick={this.toggleNewPost}>new post</button>
                        {this.props.postsData.posts.map((post) => (
                          <div key={post.id}>
                            <h3>{post.title}</h3>
                            <p>{post.content}</p>
                            <button onClick={() => this.handleEdits(post)}>
                              edit
                            </button>
                            <button
                              onClick={() => this.props.deletePosts(post.id)}
                            >
                              delete
                            </button>
                            <hr />
                          </div>
                        ))}
                      </div>
                    )}
                  </div>
                )}
              </div>
            )}
          </div>
        )}
      </div>
    );
  }
}

const mapStateToProps = (state) => ({
  // post is a key name which we used in rootReducer for postsReducer
  postsData: state.post,
});

export default connect(mapStateToProps, {
  getPosts,
  fetchPostsData,
  deletePosts,
})(Posts);

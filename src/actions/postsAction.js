import {
  GET_POSTS,
  GET_POSTS_ERROR,
  GET_POSTS_SUCCESS,
  DELETE_POSTS,
  CREATE_POSTS,
  UPDATE_POSTS,
} from "./actionTypes";
import axios from "axios";
import { tokenConfig } from "./auth";

export const fetchPostsData = (getState) => {
  return (dispatch) => {
    axios
      .get("http://127.0.0.1:8000/api/v1/", tokenConfig(getState))
      .then((response) => {
        const posts = response.data;
        dispatch(getPostsData(posts));
      })
      .catch((error) => {
        const errorData = error.message;
        dispatch(getPostsError(errorData));
      });
  };
};

// mainly for loading status true/false
export const getPosts = () => {
  return {
    type: GET_POSTS, // will be called in componentDidMount
  };
};

// fetching all posts from backend
export const getPostsData = (posts) => {
  return {
    type: GET_POSTS_SUCCESS,
    payload: posts,
  };
};

// managing api errors (mainly 404)
export const getPostsError = (error) => {
  return {
    type: GET_POSTS_ERROR,
    payload: error,
  };
};

// deleting posts from both backend and frontend
export const deletePosts = (id, getState) => {
  return (dispatch) => {
    axios
      .delete(`http://127.0.0.1:8000/api/v1/${id}/`, tokenConfig(getState))
      .then((response) => {
        dispatch({
          type: DELETE_POSTS,
          payload: id,
        });
      })
      .catch((error) => {
        const errorData = error.message;
        dispatch(getPostsError(errorData));
      });
  };
};

// to create new posts
export const createPosts = (postData, getState) => {
  return (dispatch) => {
    axios
      .post("http://127.0.0.1:8000/api/v1/", postData, tokenConfig(getState))
      .then((response) => {
        dispatch({
          type: CREATE_POSTS,
          payload: response.data,
        });
      })
      .catch((error) => {
        const errorData = error.message;
        dispatch(getPostsError(errorData));
      });
  };
};

// to update existing posts
export const updatePosts = (id, updates, getState) => {
  console.log("action recieved", id, updates);
  return (dispatch) => {
    axios
      .patch(
        `http://127.0.0.1:8000/api/v1/${id}/`,
        updates,
        tokenConfig(getState)
      )
      .then((response) => {
        dispatch({
          type: UPDATE_POSTS,
          payload: response.data,
        });
      })
      .catch((error) => {
        const errorData = error.message;
        dispatch(getPostsError(errorData));
      });
  };
};

// steps to manage authentication (setup knox and rest_framework settings in backend first)

1. a new owner field in the model.py of main application (in this case posts app)
2. manage api.py/views.py file for the permissions, get_queryset and perform_create
3. now manage user authentication, create new accounts app in django
4. create a new serializers.py file in accounts app and here manage the API's for User,
   Register and Login
5. now in the views.py (or create a new api.py) file create classes which will work on
   the API's created in serializers.py
6. create a new urls.py file and manage the urls for the api paths
7. run the commands --> python manage.py makemigrations, python manage.py migrate
8. Now the Frontend, crate new action Types in actionTypes.js (for user, login and register)
9. in the actions folder create a new authAction.js file and create user, login and
   register actions here
10. now create a new reducer file in reducer folder for auth (as authReducer.js) and manage
    reducer cases for user, login and logout
11. update the rootReducer with the authReducer as auth
12. create components based on the actions we created
13. also update the postsAction.js file with the token management.
